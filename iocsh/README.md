# IOCSH files for Digitel QPCe ion pump controller and its pump

## vac_ctrl_digitelqpce_moxa

`.iocsh` file for the controller.

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **IPADDR**
    *   The IP address or fully qualified domain name of the Moxa serial ethernet controller the pump controller is connected to
    *   string
*   **PORT**
    *   The TCP port number of the serial port on the Moxa where the pump controller is connected to (serial port 1 is usually TCP port 4001, serial port 2 is TCP port 4002, and so on)
    *   integer

## vac_pump_digitelqpce_vpi

`.iocsh` file for the pump itself

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the pump controller the pump is connected to
    *   string
*   **CHANNEL**
    *   The channel this pump is connected to on the controller. Valid values:
        *   1
        *   2
        *   3
        *   4
    *   integer
